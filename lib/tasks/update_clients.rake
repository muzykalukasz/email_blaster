namespace :db do

  task update_clients: :environment do
    puts "Updating client's records"

    require 'csv'
    csv_text = File.read(Rails.root.join('lib/assets/client_list.csv'))
    csv = CSV.parse(csv_text, :headers => true)
    Client.destroy_all
    csv.each do |row|
      Client.find_or_create_by(id: row['id']) do |x|
        x.first_name = row['first_name']
        x.last_name = row['last_name']
        x.email = row['email']
        x.is_male = row['is_male']
        x.is_formal = row['is_formal']
      end
    end

    puts 'Finished'
  end
end




