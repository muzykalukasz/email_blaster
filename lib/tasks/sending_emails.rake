namespace :emails do
  task sales: :environment do
    puts "Sending sales email"
      Client.all.each do |client|
        client.send_sales_email
        puts "- sent email to #{client.full_name} @ #{client.email}"
      end
    puts 'Finished'
  end
end