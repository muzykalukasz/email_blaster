== README

Follow the steps below to setup your application.

* $ git clone

* $ cd email_blaster

* $ bundle install

* $ rake db:migrate



Now you may want to add your CSV file to lib/assets/client_list.csv . Try with just a couple of emails - Preferably your own, so that you can see them coming through.

* once the file is ready you can update your database with:
 - $ rake db:update_clients


Now you need to update your email settings.

* go to config/environments/development.rb
* replace LOGIN and PASSWORD if you're using gmail
* if you're using other mail provider there might be more changes necessary. Like the port or authentication method


You're reday to adjust the content of your sales email:

* go to app/views/client_mailer/sales_email.html.erb
* edit the file. You have @client at your disposal.

* you can call @client.first_name, @client.last_name, @client.full_name, @client.email

You are ready you can blast emails now:

* $ rake emails:sales