class AddAttributesToClients < ActiveRecord::Migration
  def change
    add_column :clients, :is_male, :boolean
    add_column :clients, :is_formal, :boolean
  end
end
