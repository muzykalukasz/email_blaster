class ClientMailer < ActionMailer::Base
  default from: "from@example.com"


  def sales_email(client)
    @client = client
    mail(to: @client.email, subject: 'This is the title of the test email', content_type: 'text/html')
  end

end
