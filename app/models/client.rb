class Client < ActiveRecord::Base

  def full_name
    first_name + " " +last_name
  end

  def send_sales_email
    ClientMailer.sales_email(self).deliver
  end

end
